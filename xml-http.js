
// AJAX - Asynchronous JavaScript And XML

/*

<button id="button">Click</button>
<div id="container"></div>

*/

button.onclick = function() {
  container.innerHTML = 'Loading...';

  const xhr = new XMLHttpRequest();

  xhr.addEventListener('load', function() {
    container.innerHTML = xhr.responseText;
  });

  xhr.addEventListener('error', function() {
    container.innerHTML = xhr.responseText;
  });

  xhr.open('GET', 'http://10.50.93.110:8080/getText');
  xhr.send();
};
