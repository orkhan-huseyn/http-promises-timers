
/*

<button id="setTimer">Set timeout</button>
<button id="clearTimer">Clear timeout</button>

<p id="count">0</p>
<button id="startCount">Set count</button>
<button id="stopCount">Stop count</button>

<input type="text" placeholder="Write into me...">

*/


let timerId;
// let intervalId;
let animationId;

startCount.onclick = function update() {
  count.textContent++;
  // intervalId = setInterval(function() {
  //   count.textContent++;
  // }, 1000);
  animationId = requestAnimationFrame(update);
};

stopCount.onclick = function() {
  // clearInterval(intervalId);
  cancelAnimationFrame(animationId);
};

setTimer.onclick = function() {
  timerId = setTimeout(function () {
    console.log('5 seconds passed!');
  }, 5000);
};

clearTimer.onclick = function () {
  clearTimeout(timerId);
};
