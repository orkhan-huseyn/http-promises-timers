

// resolved, rejected, pending

const myPromise = new Promise(function (resolve, reject) {
  const xhr = new XMLHttpRequest();

  xhr.addEventListener('load', function() {
    resolve(xhr.responseText);
  });

  xhr.addEventListener('error', function() {
    reject('Error!');
  });

  xhr.open('GET', 'http://10.50.93.110:8080/getText');
  xhr.send();
});

myPromise
.then(function(text) {
  console.log(text);
})
.catch(function(err) {
  console.log(err);
})
.finally(() => {

});
